package com.cache;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.service.annotation.GetExchange;

public interface TodoClient {

    @GetExchange("/todos/{id}")
    Todo getTodo(@PathVariable int id);
}
