package com.cache;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.web.reactive.function.client.*;
import org.springframework.web.reactive.function.client.support.WebClientAdapter;
import org.springframework.web.service.invoker.HttpServiceProxyFactory;
import reactor.core.publisher.Mono;

@SpringBootApplication
@Slf4j
public class CacheApplication {

	public static void main(String[] args) {
		SpringApplication.run(CacheApplication.class, args);
	}

    ExchangeFilterFunction logRequest() {
        return ExchangeFilterFunction.ofRequestProcessor(clientRequest -> {
            log.info("Going to" + clientRequest.url().toASCIIString());
            return Mono.just(clientRequest);
        });
    }

    @Bean
    TodoClient todoClient(){

        WebClient client = WebClient.builder()
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .filter(logRequest())
                .build();

        HttpServiceProxyFactory proxyFactory =
                HttpServiceProxyFactory.builder(WebClientAdapter.forClient(client))
                        .build();

        return proxyFactory.createClient(TodoClient.class);
    }
}