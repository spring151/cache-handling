package com.cache;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class TodoService {

    private final TodoClient todoClient;

    public TodoService(TodoClient todoClient) {
        this.todoClient = todoClient;
    }

    @Cacheable("firstTodo") 
    public Todo getFirstTodo() {
        return todoClient.getTodo(1);
    }
}