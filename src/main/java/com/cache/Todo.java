package com.cache;

public record Todo(int userId, int id, String title, boolean completed) {
}